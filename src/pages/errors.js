import React from 'react';
import Navbar from '../components/Navbar/Navbar';
import Sidebar from '../components/Sidebar/Sidebar';
import Errors from '../components/Errors/Errors';
import {
    BrowserRouter as Router,
    Route,
    Link
} from "react-router-dom";

export default function AllDevices() {
    return (
        <React.Fragment>
            <Sidebar/>
            <div id="content-wrapper" className="d-flex flex-column w-100">
                <div id="content">
                    <Navbar/>
                    <Errors/>
                </div>
            </div>
        </React.Fragment>
    );
};
