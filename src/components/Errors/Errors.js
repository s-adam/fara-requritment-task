import React from 'react';

import {
    Link
} from "react-router-dom";

export default function Errors() {
    return (
        <div>
            <Link className="btn__errors mt-4" to="/platform-validator">10 errors</Link>
        </div>
    );
};

