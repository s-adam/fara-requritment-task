import React from 'react';

import {
    Link
} from "react-router-dom";

function handleClick() {
    const submenu = document.querySelectorAll('.collapse.show');

        const submenuArr = [...submenu];



    submenuArr.forEach(function(el) {
        el.classList.remove('show')
    });

}


function handleCollapse(){
    const submenuParent = [...document.querySelectorAll('.has-submenu')];
    const toggler = document.getElementById('toggler');
    const aside = document.querySelector('aside');
    const asideIcons = document.querySelector('#menu');


    toggler.addEventListener("click", function(){


        if(aside.classList.contains('collapsed')){
            aside.classList.remove('collapsed');
        }else{
            aside.classList.add('collapsed');
        }
    })


    asideIcons.addEventListener("click", function(){
        if(aside.classList.contains('collapsed')){
            aside.classList.remove('collapsed');
        }
    })

    submenuParent.forEach(function(el) {
        el.classList.add('collapsed')
    });
}






export default function Sidebar() {
    return (
        <aside onClick={handleClick}>
            <Link to="/" className="navbar-brand">
                <img src="logo.png" alt="Faraaaa"/>
            </Link>

                <ul className="nav flex-column flex-nowrap menu" id="menu">
                    <li className="nav-item"><Link className="nav-link d-flex align-items-center" to="#">
                        <i className="material-icons mr-3">home</i>
                        <span className="nav-item__title">Dashboard</span></Link></li>
                    <li className="nav-link">
                        <Link className="nav-item d-flex align-items-center collapsed has-submenu" to="#submenu1"
                           data-toggle="collapse" data-target="#submenu1"><i
                            className="material-icons mr-3">smartphone</i><span className="nav-item__title">Device
                            Management</span><i className="material-icons arrow-down mr-3 ml-auto">keyboard_arrow_down</i>
                        </Link>
                        <div className="collapse" id="submenu1" data-parent="#menu">
                            <ul className="flex-column pl-2 nav">
                                <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                               to="/all-devices">All
                                    Devices</Link>
                                </li>
                                <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                               to="/">New
                                    Devices
                                    <span className="alert-radius mr-3 ml-auto">2</span></Link></li>
                                <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                               to="/">Software
                                    update</Link></li>
                                <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                               to="/">Device
                                    settings</Link></li>
                            </ul>
                        </div>
                    </li>
                    <li className="nav-link"><a className="nav-item d-flex align-items-center collapsed has-submenu"
                                                href="#submenu2"
                                                data-toggle="collapse" data-target="#submenu2"><i
                        className="material-icons mr-3">directions_bus</i><span className="nav-item__title">Some
                        menu item</span><i className="material-icons arrow-down mr-3 ml-auto">keyboard_arrow_down</i></a>
                        <div className="collapse" id="submenu2" data-parent="#menu">
                            <ul className="flex-column pl-2 nav">
                                <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                            href="#">All
                                    Devices</a>
                                </li>
                                <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                            href="#">New
                                    Devices</a>
                                </li>
                                <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                            href="#">Software
                                    update</a></li>
                                <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                            href="#">Device
                                    settings</a></li>
                            </ul>
                        </div>
                    </li>
                    <li className="nav-link"><a className="nav-item d-flex align-items-center has-submenu collapsed"
                                                href="#submenu3"
                                                data-toggle="collapse" data-target="#submenu3"> <i
                        className="material-icons mr-3">settings</i>
                        <span className="nav-item__title">Settings</span><i className="material-icons arrow-down mr-3 ml-auto">keyboard_arrow_down</i></a>
                        <div className="collapse" id="submenu3" data-parent="#menu">
                            <ul className="flex-column pl-2 nav">
                                <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                            href="#">All
                                    Devices</a>
                                </li>
                                <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                            href="#">New
                                    Devices</a>
                                </li>
                                <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                            href="#">Software
                                    update</a></li>
                                <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                            href="#">Device
                                    settings</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
                <ul className="menu d-flex align-items-end h-100">
                    <li className="nav-link d-flex align-items-center mb-0">
                        <Link onClick={handleCollapse} className="nav-item d-flex" id="toggler" to="#end"> <i
                            className="material-icons rotate-90 mr-3">arrow_drop_down_circle</i> Toggle menu</Link>
                    </li>
                </ul>

        </aside>
    );
};

